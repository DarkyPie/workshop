import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Apollo, gql } from 'apollo-angular';
import { HttpLink } from 'apollo-angular/http';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  rates: any[];
  loading = true;
  error: any;

  constructor(private activatedRoute: ActivatedRoute, private apollo: Apollo, private httplink: HttpLink) { }

  ngOnInit() {

  }

  doSomeGraph() {


    this.apollo.watchQuery({
      query: gql` {product(id:"000c076c390a4c357313fca29e390ece")
      {
        id,
        sku,
        title, 
        longDescription, 
        shortDescription,   
        manufacturer {
          id
          title
        },
        categories {
          title 
        },
        price {
          price,
          currency {
            name
          }
        }
      }
    }`,
    }).valueChanges.subscribe((result: any) => {
      console.log(result);
    });
  }

  ngOnDestroy() {

  }

  ;
}
